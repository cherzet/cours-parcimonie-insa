import numpy as np
import scipy.io
from scipy import signal
import matplotlib.pyplot as plt


def fourier_transform(signal, Fs):
   """
   Compute the (discrete) fourier transform a signal with sampling frequency

   Parameters
   ----------
   signal : numpy array
      Numpy array containing the values of the signal at each time instant
   Fs : float
      Sampling frequency

   Returns
   -------
   vec_freqs: numpy array
      array of frequencies

   vec_fft: numpy array
      Array containing the values of the TF
   """

   vec_freqs = np.fft.fftshift(
      np.fft.fftfreq(signal.shape[0], 1/Fs)
   )
   vec_fft = np.fft.fftshift(np.fft.fft(signal))
   # F = np.linspace(-0.5, 0.5, signal.shape[0])*Fs


   return vec_freqs, vec_fft


def fourier_transform_positif(signal, Fe):
   FT = np.fft.fft(signal)
   F = np.linspace(0,1,signal.shape[0])*Fe

   return F, FT


def inverse_fourier_transform(FT, Fe):
   s = np.fft.ifft(np.fft.fftshift(FT))

   return s.real
 
