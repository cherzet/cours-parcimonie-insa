import numpy as np
from scipy.linalg import svdvals


def ista_miss_alg(y, D, lamb, k, mask, itermax):
    '''
    [x,e,xlasso] = ista_alg(y,D,lambda,k)
    This function computes a k-sparse coefficient vector x based on the
    Iterative Soft Thresholding Algorithm.
    ------------
    inputs:

    y : numpy array. If matrix, then the algorithm is applied to all columns
    of y independently
    D : Dictionary matrix, normalized columnwise
    lamb : regularization parameter. The larger the more x will be sparse
    (before final estimation with least squares).
    k : sparsity level of x. Choose -1 for no final projection.
    mask : a binary vector (matrix) of same size as y, stating which entries of y are unknown (0 for unknown, 1 for known).
    itermax : number of maximal iterations
    -----------
    outputs:

    x = sparse coefficient vector estimated from y.
    e = relative reconstruction error at each iteration
    xlasso = solution before final clipping
    -----------
    Credits to Jeremy Cohen, made for INSA Dictionary Learning course.
    -------------------------------------------------------------------------
    '''

    # Input caracteristics
    m, d = D.shape
    n = y.shape[1]

    # Store DtD and Dty if possible
    DtD = D.T@D
    Dty = np.zeros([d, n])
    for l in range(n):
        ind = mask[:, l]
        Dty[:, l] = np.transpose(D[ind, :])@y[ind, l]

    # Initialisation of coefficients x
    # this does not matter too much since the problem is convex
    x = np.zeros([d, n])

    # Choice of stepsize, use Lipschitz constant
    singval = svdvals(DtD)
    eta = 1/singval[0]
    # print('\nStepsize ', eta)
    # heuristic, because missing values break easy result...
    # eta = eta/1.2

    # Initial error (todo: correct with mask)
    e0 = 0
    for l in range(n):
        ind = mask[:, l]
        e0 += np.linalg.norm(y[ind, l] - D[ind, :]@x[:, l])**2
    yty = np.linalg.norm(y, 'fro')**2
    e = [np.Inf, e0/yty]

    # Initial iteration count
    iter = 0
    iter_max = itermax

    # Main print
    print('\nISTA missing values running\n')

    # Main loop with proximal gradient

    while np.abs(e[-1] - e[-2])/e[-1] > 10**(-3) and iter < iter_max:

        # printing
        if iter % 1 == 0:
            # fprintf('ISTA iteration %d\n', iter);
            print('.', end='')

        iter += 1

        err_temp = 0
        Dx = D@x

        for l in range(n):
            # Current mask
            ind = mask[:, l]
            # compute the gradient
            DtDx = np.transpose(D[ind, :]) @ Dx[ind,l]
            g = - Dty[:, l] + DtDx
            # Apply soft thresholding
            x[:, l] = x[:, l] - eta * g
            x[:, l] = np.maximum(np.abs(x[:, l])-lamb*eta, 0)*np.sign(x[:, l])

            # error computation
            err_temp += np.linalg.norm(y[ind, l] - D[ind, :]@x[:, l])**2

        # wrong error but too slow otherwise
        # err_temp = np.linalg.norm(y - D@x)**2
        e.append(err_temp/yty)
        
    e = e[1:]

    # print('\n')

    # Post-processing

    if k > 0:

        # Estimating support
        xlasso = np.copy(x)
        support = np.argsort(np.abs(xlasso), 0)
        # truncating the support
        support = support[-k:, :]
        x = np.zeros(x.shape)
        err_temp = 0

        # Running least squares
        for l in range(n):
            ind = mask[:, l]
            Dind = D[ind,:]
            indS = support[:, l]
            xls = np.linalg.lstsq(Dind[:, indS], y[ind, l], rcond=None)
            x[indS, l] = xls[0]
            err_temp += np.linalg.norm(y[ind, l] - D[ind, :]@x[:, l])**2

        e.append(err_temp / yty)
    else:
        xlasso = np.copy(x)

    return x, e, xlasso
