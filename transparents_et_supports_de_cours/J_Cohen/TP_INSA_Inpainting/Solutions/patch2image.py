import numpy as np


def patch2image(Y, dims, skip, imdim):
    '''
    Help of patch2image
    image = patch2image (Y, dimensions, skip, imdim)
    This function computes an image (np array) from a patch matrix Y, using the
    average of the values of the same pixel when they belong to several patches.
    ----------------
    inputs:

    Y : a numpy array containing the patches. Cannot have missing data. Must be in
    double format.
    dims : dimensions of the patches. Example: [10,10,3]. Third dimensions has to
    be 3 (this function is designed for RGB images or masks of missing data for
    RGB images).
    skip : ammount of shift when building the patches
    imdim : dimensions of the image. Example: [150,150,3].

    outputs:

    image : np array containing RGB intensities between 1 and 255. Pixel values are
    averaged over shared patches and floored.

    ---------------
    Credits to Jeremy Cohen. This function is designed for educational
    purpose. Made for INSA Dictionary Learning course.
    --------------------------------------------------------------------------
    '''

    # Sanity check
    if dims[2] != 3:
        print('dims[2] has to be 3')
        return

    # Initialisation of the image
    myImage = np.zeros(imdim)
    meancount = np.zeros(imdim)

    # Initialisation of counters
    x = 0
    y = 0
    p = 0

    # Building the image with superpositions
    while x+dims[0] <= imdim[0]:
        while y+dims[1] <= imdim[1]:
            # Fetching the patch
            impatch = np.reshape(Y[:, p], dims)
            # adding the patch to the image
            myImage[x:(x+dims[0]), y:(y+dims[1]), :] =  myImage[x:(x+dims[0]), y:(y+dims[1]), :] + impatch
            # counting how many times each pixel has been written on
            meancount[x:(x+dims[0]), y:(y+dims[1]), :] = meancount[x:(x+dims[0]), y:(y+dims[1]), :] + 1
            # counter
            y = y + skip
            p += 1
        x = x + skip
        y = 0

    # Averaging, clipping and rounding
    myImage = np.uint8( np.abs(myImage/meancount) / np.max(np.abs(myImage/meancount))*255 )
    # myImage = np.uint8(np.maximum(np.minimum(np.floor(myImage/meancount),255), 0))
    return myImage
