# high level imports
import os
import numpy as np
from numpy.matlib import repmat
from numpy.linalg import lstsq

# local imports
path = os.getcwd()
os.chdir(path)
#import omp
from ista_alg import ista_alg
from genDCT import genDCT


def DLmod(Y, lamb, k, dims, fact):
    """ help of DLmod
    [D,X,e,Do,Xo,ei] = DLmod(Y,lamb,k,dims,fact)
    This function computes a sparsity inducing dictionary D and a matrix of sparse coefficients X using the data stored in Y and a sparsity level k / a regularization parameter lambda for the lasso.

    The algorithm implemented is Method of Optimal Directions (MOD), which
    alternates between
       D = argmin_V ||Y - VX ||_F^2;  ||d_j||_2 = 1
     which is a quadratic problem, and
       X = argmin_B || Y - DX ||_F^2; ||x_i||_0 < k+1
     which is *solved* via convex relaxation by ISTA.
     Initialisation is carried out using a truncated DCT dictionnary, and the
     corresponding k-sparse coefficients.
    -----------------------------
     Inputs:
     Y : matrix of data
     lamb : regularization parameter
     k : sparsity level
     dims : size of the patches
     fact : (3 by 1 array) relative size of dictionary with respect to patch
     sizes.
     Outputs:
     D : output learnt dictionary.
     X : sparse matrix of coefficients.
     e : vector of reconstruction error at each iteration.
     Do : initial DCT dictionary (baseline).
     Xo : initial k-sparse coefficients for the DCT dictionary (baseline).

    ----------------------------
     Credits to Jeremy Cohen. This function is designed for educational
     purpose. Made for INSA Dictionary Learning course 2019-2020.
     Python version: 2020.
    --------------------------------------------------------------------------
    """

    # Parameters
    m, n = Y.shape

    # Initialisation of the dictionary
    # --------------------------------
    Do = genDCT(dims, fact)

    # Dictionary sizes
    # di = fact*dims
    # # Generating the DCT matrices
    # D1 = dct(np.eye(di[0]))
    # D2 = dct(np.eye(di[1]))
    # D3 = dct(np.eye(di[2]))
    # # Truncating the DCT matrices
    # D1 = D1[0:dims[0], :]
    # D2 = D2[0:dims[1], :]
    # D3 = D3[0:dims[2], :]
    # # Normalizing after truncation
    # D1 = D1*repmat(1/np.sqrt(np.sum(D1**2,0)), dims[0], 1)
    # D2 = D2*repmat(1/np.sqrt(np.sum(D2**2,0)), dims[1], 1)
    # D3 = D3*repmat(1/np.sqrt(np.sum(D3**2,0)), dims[2], 1)
    # # Creating the big dictionary (already normalized)
    # Do = np.kron(np.kron(D3, D2), D1)

    # Initialisation of the coefficients
    # ----------------------------------

    # Printing stuff
    print('The DL algorithm is computing the initial coefficients for a DCT dictionary\n')

    # Computation using OMP
    # Xo, ei = OMP(Y, Do, k);
    # Computation using ISTA
    Xo, ei, _ = ista_alg(Y, Do, lamb, k)
    print('Coefficients have been initialized\n')
    # Computing relative error (ISTA version)
    ei = ei[-1]

    # The MOD algorithm
    # -----------------

    # Parameters
    itermax = 20
    d = np.prod(fact)*m
    it = 1
    ths = 0.05*ei

    # Variable initialisation
    X = Xo
    D = Do
    e = [ei]

    # Printing
    print('Starting the main estimation loops\n')

    while it < itermax and e[-1] > ths:
        # Printing
        print('\niteration ', it)
        # error printing
        if it % 1 == 0:
            print('error \n', e[-1], '\n')

        # There might be zero rows in X; then any value in D is a minimizer. We
        # use the old one
        # Finding zero rows in X
        index = []
        for p in range(d):
            if np.sum(X[p, :] != 0) != 0:
                index.append(p)

        # D estimation as a least squares problem
        Xt = X.T
        out = lstsq(Xt[:, index], Y.T, rcond=None)
        D[:, index] = np.transpose(out[0])
        # D normalisation with safety guard
        norms = np.sqrt(np.sum(D**2, 0))
        for j, p in enumerate(norms):
            if p < 10**-8:
                norms[j] = 1
        D = D*repmat(1/norms, m, 1)

        # X estimation using OMP
        # [X,e] = OMP(Y,D,k)

        # X estimation using ISTA
        X, e_temp, Xl = ista_alg(Y, D, lamb, k)

        # Computing relative error
        #e = e/normY;
        e.append(e_temp[-1])
        # Sanity check
        #if e(end-1) < e(end)
        #    fprintf('\n ------ Error is increasing ! ---------\n try other parameters')
        #    return
        #end

         # Incrementing loop counter
        it += 1

    return D, X, e, Do, Xo, Xl
