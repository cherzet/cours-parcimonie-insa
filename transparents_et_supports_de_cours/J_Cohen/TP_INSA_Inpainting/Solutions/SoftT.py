import numpy as np


def SoftT(x, lamb):
    y = np.maximum(np.abs(x) - lamb, 0) * np.sign(x)
    return y
