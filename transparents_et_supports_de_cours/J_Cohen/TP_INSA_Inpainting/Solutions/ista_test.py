import os
import numpy as np
from numpy.matlib import repmat
from matplotlib import pyplot as plt

path = os.getcwd()
os.chdir(path)

from ista_alg import ista_alg

# Testing the OMP function


# Parameters
m = 20
k = 3
d = 30
n = 7

# Dictionary
# génération aléatoire
D = np.random.randn(m, d)
# normalisation
D = D*repmat(1/np.sqrt(np.sum(D**2, 0)), m, 1)

# Sparse true coefficients and data point
x = np.zeros([d, n])
x[1:k+1, :] = 50 + np.random.randn(k, n)
y = D@x

# Regularization parameter
lamb = 10**(-5)

# Reconstruction of x
x_est, ey, x_lasso = ista_alg(y, D, lamb, k)

# error
# x_est
ey[-1]
plt.figure()
plt.semilogy(ey)
plt.show()
