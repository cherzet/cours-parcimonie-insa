# --------------- TP Inpainting Main file ----------------
# global imports
import os
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
import numpy as np

mypath = os.getcwd()
os.chdir(mypath)

# local imports
from patches import patches
from patch2image import patch2image
from DLmod import DLmod
from ista_alg import ista_alg
from ista_miss_alg import ista_miss_alg
from genDCT import genDCT

#%% --------------- Sparse coding with ISTA ----------------

# k1=k2=k3 = 20, dims = [12,12,3], skip = 6
# lamb = 50
# ~2 minutes

# Parameters
k = 20  # 0 if you have no idea; about [30,50] seems OK
dims = np.array([12, 12, 3])  #[12, 12, 3]
skip = 6  # small integer 6
fact = np.array([1, 1, 1])  # typically 1 or 2

#%% Image inputs

# Reading the images
Castex = plt.imread('faces_gouv/castex.jpg');
Blanquer = plt.imread('faces_gouv/blanquer.jpg');
De_Montchalin = plt.imread('faces_gouv/de_montchalin.jpg');
Vidal = plt.imread('faces_gouv/vidal.jpg');
Dupond_moretti = plt.imread('faces_gouv/dupond_moretti.jpg');
# Processing with patches
P = patches(Castex,dims,skip)
P1 = patches(Blanquer,dims,skip)
P2 = patches(De_Montchalin,dims,skip)
P3 = patches(Vidal,dims,skip)
P4 = patches(Dupond_moretti,dims,skip)
# Generating dictionary
D = genDCT(dims, fact)

# Reading the mask
mask_im = np.array(mpimg.imread('faces_gouv/mask.png'))
mask_bool = mask_im[:, :, 0:3] < 0.7 # put 1 for harder problem
# Corrupting the image
Castex_tag = np.copy(Castex)
Castex_tag[mask_bool] = 0

# Building patch matrix of corrupted Castex and mask
P_corr = patches(Castex_tag, dims, skip)
mask_patch = patches(mask_bool, dims, skip)
mask_boolp = mask_patch < 0.5

# Showing the corrurpted image
plt.figure()
plt.subplot(121)
plt.imshow(Castex_tag)

# Showing the mask
plt.subplot(122)
plt.spy(mask_bool[:, :, 0])

plt.show()

#%% Testing the sparsity hypothesis


lamb = 10
X, e, Xlasso = ista_alg(P, D, lamb, k)
# Rq: it is possible to run directly SoftT columnwise

P_est = D@X
P_est_l = D@Xlasso

# Reconstructing Castex
Castex_est = patch2image(P_est, dims, skip, Castex.shape)
Castex_est_l = patch2image(P_est_l, dims, skip, Castex.shape)

# Plots and processing
plt.figure()
plt.subplot(131)
plt.title('Lasso + LS estimation')
plt.imshow(Castex_est)
plt.subplot(132)
plt.title('Lasso')
plt.imshow(Castex_est_l)
plt.subplot(133)
plt.title('Original image')
plt.imshow(Castex)

# printing sparsity level
plt.figure()
plt.subplot(121)
plt.xlabel('k for each patch')
plt.hist(np.sum(np.ones(Xlasso.shape), 0) - np.sum(Xlasso == 0, 0))
# plotting results
plt.subplot(122)
plt.semilogy(e)
plt.xlabel('cost function')

#%% Sparse coding of corrupted image

k2 = 20  #more precise fitting here if possible
lamb_2 = 50
itermax = 5
Xcorr, ecorr, Xcorrl = ista_miss_alg(P_corr, D, lamb_2, k2, mask_boolp, itermax)

# Reconstuction
P_rec = D@Xcorr
P_fill = np.copy(P_rec)
P_fill[mask_boolp] = P_corr[mask_boolp]
Castex_fill = patch2image(P_fill, dims, skip, [150, 150, 3])
Castex_est = patch2image(P_rec, dims, skip, [150, 150, 3])


plt.figure()
plt.subplot(141)
plt.imshow(Castex_tag)
plt.subplot(142)
plt.title('Filled')
plt.imshow(Castex_fill)
plt.subplot(143)
plt.title('Reconstructed')
plt.imshow(Castex_est)
plt.subplot(144)
plt.title('True Castex')
plt.imshow(Castex)
plt.show()

# Postprocessing (for hand tunning lambda)
# printing sparsity level
# printing sparsity level
plt.figure()
plt.subplot(121)
plt.xlabel('k for each patch')
plt.hist(np.sum(np.ones(Xcorrl.shape), 0) - np.sum(Xcorrl == 0, 0))
# plotting results
plt.subplot(122)
plt.semilogy(ecorr)
plt.xlabel('cost function')
plt.show()

#%% ---------------Dictionary Learning part -----------

# importing images, building the training data matrix

# Building the train data
Pt = np.concatenate([P1,P2,P3,P4],1)

# Training the dictionary

# Sparse coding steps
k_dl = 20
lamb_dl = 50  # 100
Ddl, Xdl, edl, Ddct, Xdct, Xl =  DLmod(Pt,lamb_dl,k_dl,dims,fact)

# Plot some dictionary atoms
plt.figure()
for i in range(1,5):
    for j in range(1,4):
        plt.subplot(5,5,i+5*j)
        index = np.random.randint(0, D.shape[0] )
        temp = np.reshape(D[:,index],dims)
        temp = np.uint8( np.abs(temp) / np.max(np.abs(temp))*255 )
        # temp = np.uint8(np.maximum(np.minimum(np.floor(temp),255), 0))
        plt.title(index)
        plt.imshow(temp)
        


# Reconstructed patches of Dupond_moretti
d, n = Xdl.shape
DMindex = np.int(n*3/4)
P_est = Ddl@Xdl
P4_est = P_est[:, DMindex:n]

# printing sparsity level
plt.figure()
plt.subplot(121)
plt.xlabel('k for each patch')
plt.hist(np.sum(np.ones(Xl.shape), 0) - np.sum(Xl == 0, 0))
# plotting results
plt.subplot(122)
plt.semilogy(edl)
plt.xlabel('cost function')


# Reconstructed Dupond_moretti using patch2image
Dupond_moretti_est = patch2image(P4_est,dims,skip,[150,150,3])

plt.figure()
plt.subplot(121)
plt.title('Real DupMor')
plt.imshow(Dupond_moretti)

plt.subplot(122)
plt.title('DupMor rec')
plt.imshow(Dupond_moretti_est)


#%% Estimating the missing pixels in Castex


# Sparse coding of corrupted image
k3 = 20
lamb_3 = 50
itermax = 200
Xcorr_dl, ecorr_dl, Xcorrl_dl = ista_miss_alg(P_corr, Ddl, lamb_3, k3, mask_boolp, itermax)

# Reconstuction
P_rec = Ddl@Xcorr_dl
P_fill = np.copy(P_rec)
P_fill[mask_boolp] = P_corr[mask_boolp]
Castex_fillDL = patch2image(P_fill, dims, skip, [150, 150, 3])
Castex_estDL = patch2image(P_rec, dims, skip, [150, 150, 3])

# printing sparsity level
plt.figure()
plt.subplot(121)
plt.xlabel('k for each patch')
plt.hist(np.sum(np.ones(Xcorrl_dl.shape), 0) - np.sum(Xcorrl_dl == 0, 0))
# plotting results
plt.subplot(122)
plt.semilogy(ecorr_dl)
plt.xlabel('cost function')


# Did we remove the text ?
plt.figure()
plt.subplot(231)
plt.title('DL fill')
plt.imshow(Castex_fillDL)
plt.subplot(232)
plt.title('DCT fill')
plt.imshow(Castex_fill)
plt.subplot(236)
plt.title('Tagged Castex')
plt.imshow(Castex_tag)
plt.subplot(234)
plt.title('DL rec')
plt.imshow(Castex_estDL)
plt.subplot(235)
plt.title('DCT rec')
plt.imshow(Castex_est)
plt.subplot(233)
plt.title('True Castex')
plt.imshow(Castex)



plt.show()

# Worked good with k=50,k2=50, lambda2=[50,100], kdl = 50, lambdaDL = 100, dims=[12,12,3], skip=3, fact=1 ~2min on matlab
# For python version: dims = [12,12,3], lambda=0.1, lambda_dl = 10, k = 50, skip = 6, fact = 1 ~5min
