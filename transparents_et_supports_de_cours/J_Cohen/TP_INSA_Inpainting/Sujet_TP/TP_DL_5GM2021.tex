\documentclass[11pt, oneside]{article}   	% use "amsart" instead of "article" for AMSLaTeX format
\usepackage{geometry}                		% See geometry.pdf to learn the layout options. There are lots.
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\geometry{letterpaper}                   		% ... or a4paper or a5paper or ...
\usepackage{graphicx}				% Use pdf, png, jpg, or eps§ with pdflatex; use eps in DVI mode
								% TeX will automatically convert eps --> pdf in pdflatex
\usepackage{courier}
\usepackage[T1]{fontenc}
\usepackage{amssymb,amsthm,amsmath}
\usepackage{dsfont}

%%% new colors
\usepackage[dvipsnames]{xcolor}
\definecolor{arylideyellow}{rgb}{0.91, 0.84, 0.42}
\definecolor{bananayellow}{rgb}{1.0, 0.88, 0.21}

% Defined after xcolor
\usepackage{tikz} % For Tikz plots

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\newtheorem{theorem}{Théorème}
\newtheorem{property}{Propriété}
\newtheorem{example}{Exemple}
\newtheorem{proposition}{Proposition}

% Math definitions
\DeclareMathOperator{\argmax}{argmax}
\DeclareMathOperator{\argmin}{argmin}

% Survival notations
\newcommand{\rds}[1]{\mathds{R}^{#1}}
\newcommand{\cpufont}{\fontfamily{pcr}\selectfont}
\title{TP\@: inpainting par codage parcimonieux et apprentissage de dictionnaire}
\author{}
\date{INSA 2021--2022, Jérémy Cohen}
\begin{document}
\maketitle

\section{Présentation du problème}

Par un malencontreux
accident, l'image suivante a été corrompue par un tag:

\begin{figure}[h]
        \centering
        \includegraphics[width=9cm]{tagged.jpg}%
        \label{fig:graffiti}
    \caption{Un exemple d'image corrompue par un tag.}
\end{figure}

Votre but est de restaurer les pixels manquants sur cette image.

\section{Hypothèses}

Puisque, techniquement, les pixels manquants ne sont pas observés, il est de
prime abord impossible de les retrouver. Cependant, en supposant que l'image a
certaines propriétés de régularité locale, il va être possible d'estimer ces
pixels manquants à partir de ceux qui sont connus. On peut penser, par exemple,
à un filtre moyenneur.

L'approche privilégiée dans ce TP repose sur l'information suivante : tout
petit groupe de pixels, \textit{i.e.} un patch $y_i$, est très bien décrit
par un petit nombre de coefficients $x_i$ dans un dictionnaire $D$ bien choisi
(Section~\ref{sec:patchs}).
Cette hypothèse a été pendant longtemps appliquée et vérifiée dans le cas où
$D$ est une matrices contenant des cosinus (on parle de Transformation en
Cosinus Discrète (DCT)) (Section~\ref{sec:DCT}). Dans ce TP, on va étendre ce résultat pour un
dictionnaire appris à partir d'images ressemblant à $I$ (Section~\ref{sec:dico}).

\paragraph{Objectifs}
Dans ce TP, vous devrez implémenter les fonctions suivantes:
\begin{itemize}
        \item {\cpufont SoftT }
        \item {\cpufont ista\_alg }
        \item {\cpufont ista\_miss\_alg } (avec données manquantes, nécessaire
                pour l'inpainting).
\end{itemize}
Des canevas sont fournis pour ces fonctions. De plus, un fichier {\cpufont
main.m} est fourni qui s'occupe de tout le travail auxiliaire. Il est
conseillé d'essayer de le comprendre.

Si vous avez terminé en avance, vous pouvez
\begin{itemize}
        \item faire les questions bonus.
        \item essayez de jouer avec les paramètres du problème (taille des
                patchs, paramètre de régularisation, taille du dictionnaire)
                pour faire fonctionner l'inpainting.
\end{itemize}

\section{Définition des patchs}\label{sec:patchs}

Soit $z\in [0,255]^{m_1 \times m_2 \times 3}$ une image. On peut extraire depuis $z$
une collection de petites zones de pixels appelés patchs. Un exemple de
construction de patchs est fourni par la Figure~\ref{fig:patches}. Les patchs sont des
petits rectangles de pixels, de dimension $p_1\times p_2 \times 3$ où $p_i$ est petit
devant $m_i$ (typiquement $p_1=p_2\approx 10$). Les patchs sont construits à
partir de l'image en prenant des zones qui se recouvrent, afin d'avoir de la
redondance entre les différents patches collectés.

\begin{figure}[htpb]
        \centering
        \includegraphics[width=0.9\linewidth]{patches.png}
        \caption{La construction d'une matrice de patchs.}%
        \label{fig:patches}
\end{figure}

D'un point de vue formel, on ne s'intéresse pas au fait qu'un patch soit
également une image. Cela se traduit par le fait que l'on ne stocke/manipule
les patchs que sous forme de vecteurs de taille $3p_1p_2$. Si on extrait $N$
patchs d'une image $z$, on note $P\in\mathds{R}^{3p_1p_2\times N}$ la matrices
obtenue en concaténant tous les patchs vectorizés.

Du code Python vous est fourni tel que
\[
        P = \text{{\cpufont  patches}}(z,[p_1, p_2, 3], skip)
\]
où $skip$ désigne le nombre de pixels pour la translation horizontale ou
verticale entre les patchs consécutifs. La matrice $P$ contient des nombres au
format `double'.

Il est possible de revenir à l'image à partir de ses patchs (ou d'estimer
l'image en faisant la moyenne là où les patchs se recouvrent dans le cas où les
patchs auraient été modifiés), c'est ce que fait la fonction matlab fournie
pour le TP:
\[
        z = \text{\cpufont patch2image}(P, [p_1, p_2, 3], skip, [m_1, m_2, 3])
\]

\section{Parcimonie dans un dictionnaire DCT}\label{sec:DCT}

Il a été vu dans le TP précédent que certaines (grandes) images peuvent être considérées
comme parcimonieuses dans un dictionnaire bien choisi. Cependant, ce genre
d'images simplifiées est assez rarement rencontré en pratique.

Il a été montré dans la littérature scientifique que, au contraire, les patchs
étaient bien parcimonieux dans des dictionnaires de type fréquentiels, et notamment pour des patchs issus
d'images naturelles. Ces dictionnaires permettent en effet de bien représenter les
textures avec peu de coefficients.

Dans ce TP, nous allons utiliser un dictionnaire $D$ classique en traitement
d'image: le dictionnaire DCT (discrete cosine transform). Pour simplifier, dans
le cadre de ce TP, ne cherchons pas trop à comprendre ce que l'on met dans $D$
(qui est fourni par une fonction matlab), mais rien ne vous empêche d'aller
lire sur le sujet ultérieurement.

Dans un premier temps, nous allons vérifier qu'effectivement, les patchs
$P(:,l)$ issus de la photo \textbf{non taggée} sont parcimonieux dans $D$. Pour
ce faire, vous devrez implémenter une méthode de codage parcimonieux vue en
cours.

\paragraph{Question 1 :} Implémenter la fonction {\cpufont SoftT} qui calcule
\[
        y = \text{prox}_{\lambda, \|~\|_1}(x)
\]
en autorisant des entrées matricielles. Une fonction est fournie comme caneva.

\paragraph{Question 2 :} Implémenter l'algorithme ISTA vu en cours. Une fonction
{\cpufont ista\_alg} est fournie comme caneva.

\paragraph{Question 3 :} Tester les paramètres $\lambda$ et $k$ pour que l'erreur de
reconstruction (sur tous les patchs) soit raisonable ($\frac{\|y_i-A\hat{x}_i\|_2^2}{\|y_i\|_2^2}\leq$ 10\%).
\vspace{1em}

On va maintenant pouvoir se servir de cette information pour compléter les
pixels manquants de l'image en Figure~\ref{fig:graffiti}. Pour cela, il suffit
que votre fonction ISTA tolère les pixels manquants.

\paragraph{Question 4 :} (vu en TD) écrire sur le papier l'algorithme ISTA en
présence de pixels manquants, puis modifier {\cpufont ista\_alg} en {\cpufont
ista\_miss\_alg}. Un caneva est déjà fourni.
\vspace{1em}

Le fichier {\cpufont main.m} est déjà fourni avec tout le travail de réglage
des paramètres, l'importation des images et la construction des patchs. Vous
n'avez plus qu'à tester !

\paragraph{Question bonus :} En fait, un dictionaire DCT est orthogonal si il
est carré. Avec cette information, pouvez-vous proposer une approche plus
simple pour effectuer l'inpainting?

\section{Apprentissage du dictionnaire}\label{sec:dico}
On suppose que l'on connait un ensemble de plusieurs images d'entrainement,
ressemblant à l'image taggée, et qui ne sont pas corrompue. Dans ce TP, ces
images peuvent être d'autres mini-portraits de ministres, dont les
caractéristiques physiques sont proches de celles du premier ministre.

 La question est maintenant de savoir comment mettre en place l'apprentissage
 d'un dictionnaire, c'est
 à dire obtenir une matrice $Y$ pour apprendre $D$,
conformément à ce qui a été décrit dans le cours.

Puisque la parcimonie est présente pour des patchs uniquement, on ne peut pas
travailler sur les images d'entrainement en entier. On va donc considérer que,
similairement à ce qui a été discuté plus haut,
pour toute image d'entrainement $z_j$, on est capable de construire
une matrice $P_j$ contenant en abcisse, un patch vectorisé, et en ordonnée,
l'indice de chaque patch.

On peut alors construire une matrice $P$ qui concatène les matrices des patchs
de chaque image d'entrainement. Puisque les colonnes de $P$ sont des patchs,
d'après notre hypothèse de départ, $P$ peut se décomposer approximativement de façon
parcimonieuse dans $D$. On peut ainsi chercher $\widehat{D}$ et $\widehat{X}$ tels que
\begin{equation}
        \widehat{D},\widehat{X} = \underset{D,X}
        {\argmin} \|Y - DX \|_F^2 \text{~tels que~}
        \|x_i\|_0\leq k \; \forall i\in[1,n]
\end{equation}
où $k$ est choisi par l'utilisateur. Pour ce faire, on peut utiliser
l'algorithme MOD, initialisé avec un dictionnaire DCT\@.

On obtient alors un nouveau dictionnaire $D$, que l'on espère plus adapté à
notre tâche d'inpainting.

\paragraph{Question 5 :} Utiliser le code fourni pour apprendre un dictionnaire
(tout est déjà prêt), et tracer les atomes ainsi estimés. Observer.

\paragraph{Question 6 :} Tester l'apprentissage de dictionnaire pour notre
problème d'inpainting (tout est déjà
fourni) et comparer avec le cas où $D$ était fourni. Vous pouvez vous amuser un
peu avec les paramètres.

\paragraph{Question bonus :} Reproduire le TP avec d'autres (petites) images.
Il vous suffit de trouver des images ressemblantes, d'en corrompre une (et de
connaitre le masque des pixels manquants) et d'utiliser le même canevas que
dans ce TP pour estimer les pixels manquants.

\paragraph{Question bonus :} Remplacer ISTA par un autre algorithme vu en cours
(OMP, IHT) dans une des questions précédentes.

\paragraph{Question bonus :} Comparer avec un filtre moyenneur.

%\section{Reconstruction de l'image corrompue}
%
%Armé d'un dictionnaire dans lequel les patchs similaires à ceux de l'image $I$
%corrompue sont parcimonieux, il reste maintenant à savoir comment fournir une
%estimation des valeurs des pixels manquants.
%
%Soit $M$ le masque des pixels manquants, c'est à dire le tableau de même taille
%que $I$ contenant des $1$ pour les pixels connus, et des $0$ pour les pixels
%manquants. On peut construire une matrice $Y_{mask}$ qui contient des
%``patches'' de 0 et de 1. Soit $Y_{tag}$ la matrice de patchs obtenue à partir
%de $I$. On cherche en fait à résoudre le problème
%suivant:
%\begin{equation}
%        \widehat{X}_{tag} = \underset{X, \|x_i\|_0\leq k}{\argmin}
%        \|Y_{mask}\ast\left(Y_{tag} -
%        \widehat{D}X\right)\|_F^2
%\end{equation}
%où $\ast$ est un produit terme à terme. Ici, on ne calcule en fait l'erreur que
%sur les pixels connus.
%
%Finalement, une fois $X_{tag}$ estimé, on peut reconstruire les pixels manquant
%de $Y_{tag}$ en utilisant le fait que $\widehat{D}$ et $\widehat{X}_{tag}$ sont
%pleins. En notation MATLAB pour la deuxième ligne,
%\begin{align}
%        \widehat{Y} &= \widehat{D}\widehat{X_{tag}} \\
%        \widehat{Y}(Y_{mask}) &= Y_{tag}(Y_{mask})
%\end{align}
%Ici, on utilise l'image reconstruite uniquement pour remplir les pixels
%manquants dans tous les patchs.
%
%Enfin, on peut obtenir une image $I_{clean}$ en reconstruisant une image à
%partir des patchs $\widehat{Y}$. Puisque les patchs peuvent se superposer, on moyenne les
%différentes valeurs du même pixel lorsqu'il est présent sur différents patchs.
%
\end{document}
