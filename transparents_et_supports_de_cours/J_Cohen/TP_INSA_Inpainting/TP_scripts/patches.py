import numpy as np


def patches(myImage, dims, skip):
    ''' Help of patches
     Y = patches (image, dimensions, skip)
     This function computes a patch matrix Y from image, which is a (RGB) color
     image stored in a 3-way array. The user can specify the size of the patches
     dims (the third dimensions has to be 3), and the
     overlap of patches by specifying the amount of shift pixelwise between each
     patch.
    -----------------
     Inputs:

     image : a numpy array containing the image. May be a mask of missing data.
     dims : dimensions of the patches. Example: [10,10,3]. Third dimensions has to
     be 3 (this function is designed for RGB images or masks of missing data for
     RGB images).

     outputs:

     Y : matrix containing vectorized patches column-wise. Stored in double
     format.

    ----------------
     Credits to Jeremy Cohen. This function is designed for educational
     purpose. Made for INSA Dictionary Learning course.
    ---------------------------------------------------------------------------
    '''

    # Sanity check
    if dims[2] != 3:
        print('dims[2] has to be 3')
        return

    # Parameters
    m1, m2, _ = myImage.shape

    # Initialisation of Y
    Y = np.zeros([np.prod(dims), 1])

    # Initialisation of counters
    x = 0
    y = 0

    # Building the patches
    while x+dims[0] <= m1:
        while y+dims[1] <= m2:
            impatch = myImage[x:(x+dims[0]), y:(y+dims[1]), :]
            Y = np.concatenate((Y, np.reshape(impatch, [np.prod(dims), 1])), axis = 1)
            y = y + skip
        x = x + skip
        y = 0
    Y = np.double(Y)
    # Removing the first zero column (dirty trick)
    Y = Y[:, 1:]
    return Y
