import numpy as np
from scipy.linalg import svdvals


def ista_alg(y, D, lamb, k):
    '''
    [x,e,xlasso] = ista_alg(y,D,lambda,k)
    This function computes a k-sparse coefficient vector x based on the
    Iterative Soft Thresholding Algorithm.
    ------------
    inputs:

    y : numpy array. If matrix, then the algorithm is applied to all columns
    of y independently
    D : Dictionary matrix, normalized columnwise
    lamb : regularization parameter. The larger the more x will be sparse
    (before final estimation with least squares).
    k : sparsity level of x. Choose -1 for no final projection.
    -----------
    outputs:

    x = sparse coefficient vector estimated from y.
    e = relative reconstruction error at each iteration
    xlasso = solution before final clipping
    -----------
    Credits to Jeremy Cohen, made for INSA Dictionary Learning course.
    -------------------------------------------------------------------------
    '''

    # Input caracteristics
    m, d = D.shape
    n = y.shape[1]

    # Store DtD and Dty if possible
    DtD = D.T@D
    Dty = D.T@y

    # Initialisation of coefficients x
    # this does not matter too much since the problem is convex
    x = np.zeros([d, n])

    # Choice of stepsize, use Lipschitz constant
    eta = 0  # xxx TODO

    # Initial error
    e0 = 0  # xxx TODO
    # e_old = 0
    e = [np.Inf, e0]
    yty = np.linalg.norm(y, 'fro')**2

    # Initial iteration count
    iter = 0
    iter_max = 50

    # Main print
    print('ISTA running\n')

    # Main loop with proximal gradient

    while np.abs(e[-1] - e[-2])/e[-1] > 10**(-3) and iter < iter_max:

        # printing
        if iter % 10 == 1:
            # fprintf('ISTA iteration %d\n', iter);
            print('.', end='')

        iter += 1

        # compute the gradient
        g = 0  # xxx TODO
        # Apply soft thresholding
        x = 0  # xxx TODO
        x = 0  # xxx TODO

        # error computation
        e.append(0 / yty)  # xxx TODO

    e = e[1:]

    # print('\n')

    # Post-processing

    if k > 0:

        # Estimating support
        xlasso = np.copy(x)
        support = np.argsort(np.abs(xlasso), 0)
        # truncating the support
        support = support[-k:, :]
        x = np.zeros(x.shape)

        # Running least squares
        for l in range(n):
            xls = 0  # xxx TODO, xls is the output
            indices = support[:, l]
            x[indices, l] = xls[0]

        e.append(0 / yty)  # xxx TODO
    else:
        xlasso = np.copy(x)

    return x, e, xlasso
