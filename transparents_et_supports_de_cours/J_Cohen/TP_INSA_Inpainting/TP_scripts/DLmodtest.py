# Test of DLmod

# global imports
import os
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
import numpy as np

# adding local path
#dirname = os.path.dirname(os.path.realpath(__file__))
#print(dirname)

mypath = os.getcwd()
os.chdir(mypath)

# local imports
from patches import patches
from patch2image import patch2image
from DLmod import DLmod
#import OMP_missing



# Testing the initialisation

# Importing a test image
Castex = np.array(plt.imread(dirname+'/faces_gouv/castex.jpg'))

# Parameters
k = 50
dims = [15, 15, 3]
skip = 5
fact = 1

# Building a matrix of data patches
Y = patches(Castex, dims, skip)

# Testing the function
# Sparse coding steps
lamb = 10**(2)
out = DLmod(Y, lamb, k, dims, fact)
#[Ddl,Xdl,edl,Ddct,Xdct,ei] =

# initialisation test
# Reconstructed patches
Ddct = out[3]
Xdct = out[4]
Ydct = Ddct@Xdct
# Reconstructed image
Castexdct = patch2image(Ydct, dims, skip, [150, 150, 3])


# Learnt dico test
# Reconstructed patches
Ddl = out[0]
Xdl = out[1]
Ydl = Ddl@Xdl
# Reconstructed image
Castexdl = patch2image(Ydl, dims, skip, [150, 150, 3])
# Showing reconstructed Castex and real one
# Showing reconstructed Castex and real one
plt.figure()
plt.title('True Castex')
plt.imshow(Castex)

plt.figure()
plt.title('Castex DCT')
plt.imshow(Castexdct)

plt.figure()
plt.title('Castex Dictionary Learning')
plt.imshow(Castexdl)

plt.show()

## Straightforward inpainting, with D learnt on Castex
##(hide for TP)
#
## Loading the corrupted image and mask
##Castex_tag = plt.imread(dirname+'/faces_gouv/castex_marked.jpg')
#mask_im = np.array(mpimg.imread(dirname+'/faces_gouv/mask.png'))
#mask_bool = mask_im[:, :, 0:3] < 1
## Corrupting the image
#Castex_tag = np.copy(Castex)
#Castex_tag[mask_bool] = 0
#
#plt.figure()
#plt.imshow(Castex_tag)
#
#
## Building patch matrix of corrupted Castex and mask
#Y_corr = patches(Castex_tag, dims, skip)
#
## Sparse coding of corrupted image
#k2 = 50
#Xcorr, ecorr = OMP_missing(Y_corr, Ddl, k2, mask_bool)
#ecorr = ecorr/np.linalg.norm(Y_corr, 'fro')**2
#
## Reconstuction
#Y_rec = Ddl@Xcorr
#Y_fill = np.copy(Y_rec)
#Y_fill[mask_bool] = Y_corr[mask_bool]
#Castex_fill = patch2image(Y_fill, dims, skip, [150, 150, 3])
#
#
## Did we remove the text ?
#plt.figure()
#plt.title('Reconstructed Castex')
#plt.imshow(Castex_fill)
#
## Worked Ok-ish with k=50=k2, dims=[10,10,3], skip=2, fact=2
## Worked great with k=25, k2=50, dims=[15,15,3], skip=3, fact=1
